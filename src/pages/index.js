import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import HomepageFeatures from '@site/src/components/HomepageFeatures';

import styles from './index.module.css';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary ', styles.heroBanner, styles.heroHomepage)}>
      <div className="container">
        <h1 className="hero__title">{siteConfig.title}</h1>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className="welcome-container">
         <img src='/img/welcome-guy.jpg'/>
        <p className="hero_welcome">Welcome to the Control API documentation for Voicemod! 
        <br/>
        This documentation is intended to provide developers with everything they need to know to integrate Voicemod's powerful voice changing and modulation features into their own applications. With the Control API, developers can add real-time voice effects, modify the user's voice during recordings, and create engaging user experiences that stand out from the crowd.
        </p>
        </div>
        <div className={clsx(styles.buttonsList)}>
          <div className={'buttons'}>
            <Link
              className="button button--secondary button--lg"
              to="https://voicemod.typeform.com/to/Zh5ZHRED">
              Get your API Key 🗝️
            </Link>
          </div>
  
          <div className={'buttons'}>
            <Link
              className="button button--primary button--lg"
              to="/getting-started">
              Get started in 2min ⏱️
            </Link>
          </div>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="With the ControlAPI you can add real-time voice effects and filters by remote-controlling the Voicemod application">
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}
