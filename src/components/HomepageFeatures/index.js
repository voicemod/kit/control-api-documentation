import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';
import Link from '@docusaurus/Link';

const FeatureList = [
  {
    title: 'Easy to Use',
    img: '/img/easy-to-use.jpg',
    description: (
      <>
        The Control API provides a simple, yet, powerful interface for you to interact with and use the Voicemod application however you want.
      </>
    ),
  },
  {
    title: 'Real-time voice changer',
    img: '/img/real-time-changer.jpg',
    description: (
      <>
      The Control API allows you to change and customize your voice in real time. Integrate that into your own application to react to events and adjust your user's voice accordingly.
      </>
    ),
  },
  {
    title: 'Cross platform support',
    img: '/img/multi-platform.jpg',
    description: (
      <>
        Use any technology you want to interact with our Control API. Are you building using Unity? JavaScript? C/C++? Java? We support them all!
      </>
    ),
  },
];

function Feature({Svg, img, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        {Svg && <Svg className={styles.featureSvg} role="img" />}
        {img && <img className={styles.featureImg} role="img" src={img} />}
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}

          
        <div className='container'>
        <div className={'buttons'}>
          <Link
            className="button button--secondary button--lg"
            to="/getting-started">
            Get started now️
          </Link>
        </div>
</div>
        </div>
      </div>
    </section>
  );
}
