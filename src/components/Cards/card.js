import Link from '@docusaurus/Link';
import React from 'react';
import styles from './styles.module.css'


export default function Card({title, text, href, size=50}) {

    let sizeStyles = "";
    if(size == 33) {
        sizeStyles = styles.customCard33
    } 
    if(size == 50) {
        sizeStyles = styles.customCard50
    }

    return (
        <div className={["container", styles.customCard, sizeStyles].join(" ")}>
            <p className={styles.cardTitle}>
                <Link to={href}>{title}</Link>
            </p>

            {text && (<p className={styles.customCardText}>
                {text}
            </p>)}
        </div>
    )
}