// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const dotenv = require('dotenv')


dotenv.config()


const TITLE = 'Voicemod - Control API - Documentation'
const TAG_LINE = 'Control Voicemod App from your own application'

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: TITLE,
  tagline: TAG_LINE,
  favicon: 'img/favicon.svg',

  // Set the production url of your site here
  url: 'https://control-api.voicemod.net',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'voicemod', // Usually your GitHub org/user name.
  projectName: 'control-api-docs', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/voicemod/kit/control-api-documentation/-/blob/main/',
        },
        blog: false, //{
   //       showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
         // editUrl:
//            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
 //       },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      colorMode: {
        defaultMode: 'dark',
        disableSwitch: false,
        respectPrefersColorScheme: false
      },
      docs: {
        sidebar:{
          autoCollapseCategories: false
        } 
      },
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        style: 'dark',
        title: TITLE,
        logo: {
          alt: TAG_LINE,
          src: '/img/favicon.svg',
        },
        items: [
          //{
            //type: 'docSidebar',
            //sidebarId: 'tutorialSidebar',
            //position: 'left',
            //label: 'Tutorial',
          //},
          {
            href: 'https://gitlab.com/voicemod/kit/control-api/',
            label: 'Gitlab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          //{
            //title: 'Docs',
            //items: [
              //{
                //label: 'Tutorial',
                //to: '/docs/intro',
              ////},
            //],
          //},
          {
            title: 'Community',
            items: [
              {
                label: 'Voicemod Developer Portal',
                href: 'https://www.voicemod.net/developers/'
              },
              {
                label: 'Discord Community',
                href: 'https://discord.gg/vm-dev-community',
              },
 
              {
                label: 'Stack Overflow',
                href: 'https://stackoverflow.com/questions/tagged/voicemod',
              },
             {
                label: 'Twitter',
                href: 'https://twitter.com/voicemod',
              },
            ],
          },
          //{
            //title: 'More',
            //items: [
              //{
                ////label: 'Blog',
                //to: '/blog',
              //},
              //{
                //label: 'GitHub',
                //href: 'https://github.com/facebook/docusaurus',
              ////},
            //],
          //},
        ],
        copyright: `Copyright © ${new Date().getFullYear()} This documentation was built with Docusaurus and ❤️ by Voicemod© for developers`,
      },
       algolia: {
      // The application ID provided by Algolia
      appId: process.env.PUBLIC_ALGOLIA_APP_ID,

      // Public API key: it is safe to commit it
      apiKey: process.env.ALGOLIA_SEARCH_KEY,

      indexName: process.env.PUBLIC_ALGOLIA_INDEX,

      // Optional: see doc section below
      contextualSearch: false,

      // Optional: Specify domains where the navigation should occur through window.location instead on history.push. Useful when our Algolia config crawls multiple documentation sites and we want to navigate with window.location.href to them.
      externalUrlRegex: 'external\\.com|domain\\.com',

      // Optional: Replace parts of the item URLs from Algolia. Useful when using the same search index for multiple deployments using a different baseUrl. You can use regexp or string in the `from` param. For example: localhost:3000 vs myCompany.com/docs
      replaceSearchResultPathname: {
        from: '/docs/', // or as RegExp: /\/docs\//
        to: '/',
      },

      // Optional: Algolia search parameters
      searchParameters: {
        facetFilters: null,
        attributesToRetrieve: ['url', 'title', 'content', 'hierarchy.lvl0', 'hierarchy.lvl1']
      },


      // Optional: path for search page that enabled by default (`false` to disable it)
      searchPagePath: 'search',

      //... other Algolia params
    },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
