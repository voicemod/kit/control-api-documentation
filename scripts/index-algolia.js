//const algoliasearch = require('algoliasearch')
import algoliasearch from 'algoliasearch'
import { readFile } from 'fs'
import dotenv from 'dotenv'
//const {readFile} = require('fs')
//const dotenv = require('dotenv')

dotenv.config()

console.log(process.env)

const DOCUMENTATION_SITE_URL = "http://localhost:3000"

// API keys below contain actual values tied to your Algolia account
const client = algoliasearch(process.env.PUBLIC_ALGOLIA_APP_ID, process.env.ALGOLIA_ADMIN_SECRET);
const index = client.initIndex(process.env.PUBLIC_ALGOLIA_INDEX);

readFile('./scripts/search-index.json', (err, data) => {
    if(err) {
        console.error("Error reading JSON index file: ")
        console.error(err)
        return 1;
    }
    try {
        const jsonData = JSON.parse(data.toString())
        let objects = []

        jsonData.forEach( page => {
            console.log("Trying to index ", page.slug)
            objects.push({
                objectID: page.slug,
                title: page.title,
                hierarchy: { lvl0: page.title, lvl1: page.title},
                url: DOCUMENTATION_SITE_URL + "/" + page.slug,
                content: page.body
            })
        })
        const chunkSize = 500;
        for (let i = 0; i < objects.length; i += chunkSize) {
            const chunk = objects.slice(i, i + chunkSize);
            // do wher
            index.saveObjects(chunk)
            .then( ({objectIDs}) => {
                console.log(objectIDs.length, " elements indexed")
            }).catch( err => {
                console.log(err)
                console.log(err.transporterStackTrace)
            })
        }    
        console.log("Process done")
    } catch (e) {
        console.error("Error parsing index JSON data")
        console.error(e)
    }
})