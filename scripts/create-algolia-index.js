import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import matter from 'gray-matter';
import slugify from 'slugify';
import { EOL } from 'os';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const sourceDir = path.join(__dirname, '..', 'docs');

function readFilesRecursively(directory) {
	const files = [];
	const entries = fs.readdirSync(directory, { withFileTypes: true });

	entries.forEach((entry) => {
		if (entry.isDirectory()) {
			files.push(...readFilesRecursively(path.join(directory, entry.name)));
		} else if (entry.isFile() && (entry.name.endsWith('.md') || entry.name.endsWith('.mdx')) ) { //only target the MD or MDX files
			files.push(path.join(directory, entry.name));
		}
	});

	return files;
}

function parseAPIRefFile(content) {
    content = content.split("\n")
    let sections = []
    let sectionCounter = -1

    content.forEach( (line) => {
        if(line.indexOf("### ") == 0) {
            sectionCounter++
            sections[sectionCounter] = [//create a fake frontmatter for this section of the file
                '---',
                'title: ' + line.replace("###", "").replace(/`/g, ''),
                '---',
                ' '
            ]
        } 
        if(sectionCounter >= 0) {
            sections[sectionCounter].push(line)
        }
    })
    return sections
}


function parseFile(fileContent, sourceDir, filePath, isSection=false) {
    const {
		data: { title },
		content,
	} = matter(fileContent);

	let slug = path
		.relative(sourceDir, filePath)
		.replace(/\\/g, '/')
		.replace(/\.mdx$/, '')
		.replace(/\.md$/, '');
    
    if(slug.indexOf("/index") != -1) {
        slug = slug.replace('/index', '')
    }
    
    if(isSection) {
        slug += '#' + slugify(title.toLowerCase())
    }

    return {
        title,
        content,
        slug
    }

}

const inputPaths = readFilesRecursively(sourceDir);
let indexData = inputPaths.map((filePath) => {
	const fileContent = fs.readFileSync(filePath, 'utf-8');
    if(filePath.indexOf('api-reference') != -1) {
        let sections = parseAPIRefFile(fileContent)
        return sections.map( section => {
            const {slug, content, title} = parseFile(section.join(EOL), sourceDir, filePath, true)
            return {
                slug,
                category: 'documentation',
                title: title,
                body: content,
            };
        })
    } else {
        const {slug, content, title} = parseFile(fileContent, sourceDir, filePath)
        
        return {
            slug,
            category: 'documentation',
            title: title,
            body: content,
        };
    }
});

indexData = indexData.flat()

// fs.writeFileSync('public/search-index.json', JSON.stringify(indexData, null, 2));
fs.writeFileSync('./scripts/search-index.json', JSON.stringify(indexData));