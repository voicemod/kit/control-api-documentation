---
title: What are soundboards?
---

A soundboard essentially a category of [sounds](/sounds).

Soundboards are great to keep your sounds organized and easy to find. If you're using the ContrlAPI to integrate and incorporate your sounds into
another application, making use of soundboards might be a great idea.

## Getting all soundboards

To get a list of all soundboards available to you from the app, you'll use the `getAllSoundboard` message.

The message looks like this:

```json
{
  "action": "getAllSoundboard",
  "id": "[unique message id]",
  "payload": {}
}
```

As a result, the server will emite a `getAllSoundboard` event with the following structure:

```json
{
  "actionType": "getAllSoundboard",
  "actionObject": {
    "soundboards": [
      {
        "id": "[internal soundboard id]",
        "name": "[human friendly name]",
        "isCustom": true,
        "enabled": true,
        "sounds": [
            //list of sounds for this soundboard here...
        ]
      }
      //...
    ]
  }
}
```

## Data structure of a soundboard
The received data contains pretty much all you need to capture the soundboard _and_ the sounds inside it.
The basic structure of the soundboard data type is:

- `id (string)` : This is an internal alphanumeric ID to identify each individual soundboard.
- `name (string)` : This is human friendly name which you can use if you want your users to identify the sound by name.
- `isCustom (boolean)`: This flag is `true` if this soundboard was created by the user, otherwise it'll be set to `false`.
- `enabled (boolean)`: This flag is `true` if the soundboard (and its sounds) can be used by the client application (your application). If the flag is set to `false` sounds may not play, so avoid using it.
- `sounds (list of sounds)`: Here is where the data for each sound inside the soundboard reside.

## Data structure of a sound inside the soundboard
The structure for the sounds listed inside each soundboard is the following:

```json
{   
    "id": "ad6efa6b-4872-4217-9465-5415178dfc1f",
    "name": "Nuke Alert",
    "isCustom": false,
    "enabled": true,
    "playbackMode": "PlayRestart",
    "loop": false,
    "muteOtherSounds": false,
    "muteVoice": false,
    "stopOtherSounds": true,
    "bitmapChecksum": "14-04-DB-12-50-09-25-14-18-C5-62-2D-4A-AB-8D-31"
}
```

- `id (string)`: The unique ID of the sound (also known as `Filename` if you're using the `getMemes` endpoint), see the [playing a sound](/sounds/playing-sounds) section to know how to use it.
- `name (string)` : The human-friendly name identifying the sound.
- `isCustom (boolean)`: If this sound was made by the user, this flag will be set to `true` otherwise, `false`.
- `enabled (boolean)`: Set to `true` if the sounds can be played, otherwise it'll be listed but won't work (and the flag will be set to `false`)
- `playbackMode (string)`: The playback mode for this sounds. Refer to the [re-playing sounds](/sounds/playing-sounds#re-playing-a-sound) section to understand the different values.
- `loop (boolean)`: This flag indicates whether or not the sound will keep playing once it's done. If it's `true` then it'll never stop until you use the  `stopAllMemeSounds` message ([see here](/sounds/stopping-all-sounds) for more details).
- `muteOtherSounds (boolean)`: If set to `true` when playing this sound, all other active sounds will be muted until this sound ends. Once the sound ends, the other sounds will be heard again.
- `muteVoice (boolean)`: If set to `true` this sound will mute your voice while it plays, once the sounds ends, your voice will be unmuted.
- `stopOtherSounds (boolean)`: If set to `true` all active sounds will be stopped (not muted) when this sound starts playing.
- `bitmapChecksum (string)`: This hash value can be used to validate the bitmap returned by the `getBitmap` message ([see here](/sounds/getting-bitmap) for more details on that message).
