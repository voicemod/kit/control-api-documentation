---
title: Reading your active soundboard
---

While you can always request **all soundboards** with the `getAllSoundboard` message ([see here](/soundboards/what-are-soundboards#getting-all-soundboards) for more details), through the 
Voicemod app, the user can only have one soundboard active at any given time.
And every time they select a new soundboard, the API will emit an event. 
You can listen for that event and react to the change.

The event triggered is: `getActiveSoundboardProfile`

The structure of that message is the following:

```json
{
  "type": "getActiveSoundboardProfile",
  "payload": {
    "profileId": "[ID of the currently active profile]"
  }
}
```
