---
title: Get the sound's bitmap
---
# Getting the bitmap of a sound

Each sound has an associated bitmap (an image) that represents it. 
That image is useful if you’re building a UI around the Control API and you want a graphical representation of each sound.

The following image showcases what a bitmap looks like.

<div class="doc-img-container">

![Voice filter Bitmaps](../img/sound-icon.png)

</div>
To get the bitmap for a particular sound, you’ll use the `getBitmap` command that looks like this:

```json
{
  "action": "getBitmap",
  "id": "[a unique id]",
  "payload": {
    "memeId": "[the sound ID of your choice]"
  }
}
```

The API in return, will issue an event with the name of `getBitmap`, so make sure your client app is waiting for it.

The structure of the event looks like this:

```json
{
  "actionType": "getBitmap",
  "actionID": "[the unique ID you used in your message]",
  "appVersion": "2.0.0.34",
  "actionObject": {
    "result": {
        "default": "iVBORw0KUuxZaGehlmhmkW50Ng9dM [...] =="
    },
    "memeId": "[the sound ID your requested the bitmap for]"
  }
}

```

Out of all that information, here is what you should know:

- `actionID`: This is the `id` you sent as part of the original `getBitmap` message. You can use this information to match the response to the correct voice, specially so if you’re requesting several at the same time.
- `actionObject.default`: The bitmap encoded information for the default icon for this sound. You have the GIF data of the image here encoded in base64, it’s up to you to display it based on the tech stack of your choice.


