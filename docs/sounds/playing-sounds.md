---
title: Playing a Sound
---

# Playing a Sound

To start a sound (i.e. to play it), you’ll use the `playMeme` message.

The structure of the message is:

```json
{
  "action": "playMeme",
  "id": "[a unique id for your message]",
  "payload": {
    "FileName": "[the filename value of the meme you're looking to play]",
    "IsKeyDown": true
  }
}
```

You’re essentially having to send the `Filename` from the list of Sounds.

As a result, the Sound will start playing and you won’t get an event triggered by the server, so you don’t have to look for anything special.

## Re-playing a Sound

While this doesn’t happen with voice filters, when a Sound is playing and you try to play it again, you’ll get different results, depending on the Sound.

If you look at the data structure of Sound, you’ll notice there is a property called `Type`, that value indicates how will the Sound behave if you try to play it while it’s already being played.

The potential values for the `Type` property are:

- **PlayPause**: This type of Sound will be paused whenever you try to play it and it’s already being played. This means that if you play them for a third time, the sound will continue from where it was before.
- **PlayStop**: These Sounds will stop if you try to play them and they’re already playing. If you try to play them for a third time, they’ll start from the start.
- **PlayRestart**: If you try to play these Sound while they’re already playing, they’ll simply start over.
- **PlayOverlap**: For these Sound, if you try to play them and they're already playing, they'll play on top of the already playing sound. In other words, the Sound will overlap itself starting at the beginning while the previous version keeps playing.
- **PlayLoopOnPress**: These Sounds will play on loop once you start them and the only way to stop them is through the `stopAllMemeSounds` message.