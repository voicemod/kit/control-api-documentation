---
title: Getting the list of Sounds
---

# Getting the list of Sounds

To get the full list of Sounds you’ll use the `getMemes` message, which in return sparks a `getMemes` event from the server.

The structure of the message you need to send from your app is the following:

```json
{
  "action": "getMemes",
  "id": "[a unique id for the message]",
  "payload": {}
}
```

And the response you’ll get in the form of an event triggered by the server is the following:

```json
{
  "actionType": "getMemes",
  "appVersion": "2.41.0.0",
  "actionID": "[your unique message id]",
  "actionId": "[your unique message id]",
  "actionObject": {
    "listOfMemes": [
      {
        "FileName": "4f64e0b5-6187-4c48-8975-946224e142a4",
        "fileName": "4f64e0b5-6187-4c48-8975-946224e142a4",
        "Profile": "Prankster",
        "profile": "Prankster",
        "Name": "Alert",
        "name": "Alert",
        "Type": "PlayRestart",
        "type": "PlayRestart",
        "IsCore": true,
        "isCore": true
      },
     //.... more sounds here
    ]
  },
  "context": null
}
```
:::info
You might be surprised by the duplication of the properties. Please note that this is purely for backwards compatibility with older clients. The information present in both versions of the same property will be __exactly the same__.
:::

Keep in mind that there is no pagination for the list of results returned, which means you might be getting a lot of sounds all at once. Use this message at your own discretion.