---
title: Getting started with the Control API
---
import Card from '../src/components/Cards/card.js'

# Getting started 
If you already have the Windows Voicemod application installed, you can start using the Voicemod Control API right away, without having to download or install anything else.

:::tip Getting your  API Key 🗝️
Just fill in this [online form](https://voicemod.typeform.com/to/Zh5ZHRED) to request an **API Key**, you should receive it in your inbox in no time.
:::

If you don't have the Voicemod app yet, be sure to [download it](https://www.voicemod.net/downloadVoicemodAccount.php) before proceeding with next steps.


:::note Review our Sample app
If you'd like to see a practical implementation of an API client in JavaScript, we have a sample [project here](https://gitlab.com/voicemod/kit/control-api) that you can test yourself.
:::

## The message-event flow
It’s essential to understand how to communicate with the Control API to fully comprehend the rest of this documentation.

As a developer, you will interact with the API through a **client application**. This client app will connect to the API (also known as **the server**), and the client will send messages to it. 
The structure of every message will be similar, in that they will all have the following properties:


-  `id` (`string`): Any alphanumeric string that can be used to identify the message. 

:::tip
For each message that triggers an event on the server (not all messages do), the event sent from the server will have the same `id`, so you can match the response to the originating message.
:::

- `action` (`string`): The string identifying the action you want to perform.
- `payload` (`object`): A JSON containing extra information about the message you're sending.

As a result of your message, if the server needs to send some information back, a new event will be triggered.
While __the structure of each event's payload is not standard__, you'll get access to an `id` event which will correspond to the `id` value of the message that triggered the event.

## Connecting to the API

You can access the Voicemod Control API via sockets on one of the following ports, at the path: `/v1`

```jsx
59129, 20000, 39273, 42152, 43782, 46667, 35679, 37170, 38501, 33952, 30546
```

You’ll need to create the following URL to connect to the API: `ws://localhost:[port]/v1`

:::tip
As we have so many potential ports, we suggest you cycle through all them if there is a connection issue until you’ve gone through all of them.
:::

Once a connection is established, your application must send the following message to make the app server aware of its presence. 

:::caution Remember!
This must be the first message you send once connected.
:::

The message’s name is `registerClient` and has the following structure:

```json
{
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "action": "registerClient",
  "payload": {
    "clientKey": "<my API key>"
  }
}
```

At this point you’re now ready to start using the Control API!

## Sending messages to the Control API
The messages your client application will send to the Control API will always be in JSON format, and the structure will always be the same.
They must contain the following properties:

- `action`: A string identifying the name of the action to perform (ie. `registerClient` , `loadVoice` , etc).
- `id`: A unique identifier set by the client application. This value will be used by the API as part of  the response body. Client applications can use this property to keep track of sent messages and the corresponding asynchronous response.
- `payload`: A JSON object containing message-specific properties. This section of the message will vary depending on the action you’re trying to perform.

## Setting up a Voice filter

The concept of “voice filter” is that of a digital filter applied in real-time to the sound detected by the Voicemod app from the configured input device.

In less technical words, “voice filters” will let you change way you sound when you speak into the microphone.

If you want to know more about voice filters, check out [the Voice Filters Section](/voices) of this documentation.

The message you’ll have to send to load a different filter is the `loadVoice` message:

```json
{
  "action": "loadVoice",
  "id": "[a unique id chosen by the client]",
  "payload": {
    "voiceID": "[the voice ID to be used]"
  }
}
```

The `voiceID` property of the payload on the message is going to be a unique string identifying the voice filter you’re looking to load. You can get the list of all available filters through the `getVoices` message.

To get you started, you can try the `cave` or the `robot` ids. 

:::tip Remember!
For this to work, you’ll have to have the “Voice Changer” switch enabled on the Voicemod app, otherwise your message won’t have any effect.

And if you also want to hear how you sound after the message, you’ll have to enable the “Hear Myself” switch as seen in the below image:

![Application Switches](./img/app-switches.png)

:::
## Stop the voice filter

To stop the voice changer from affecting your voice, you an always disable the “Voice Changer” switch directly from the app, or you can use the `nofx` special voice filter ID.

In other words, you have to send another `loadVoice` message, but this time around, you’ll use the `nofx` filter ID:

```json
{
  "action": "loadVoice",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "voiceID": "nofx"
  }
}
```

That’s it! You’re officially in business!

If you’d like to do more with the API, visit the following sections

<Card title="Learn about Voice filters" href="/voices/" text="Make yourself sound like a robot or a barbarian. Learn how to change your voice in real-time through the Control API. " size="50"/>
<Card title="Learn about Sounds" href="/sounds/" text="Sounds add sounds effects on top of your voice, learn how to control them." size="50"/>