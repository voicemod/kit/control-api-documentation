---
title: Getting all voice filters
---

# Getting all available voice filters

To get the full list of available voice filtes (keep in mind they can change regularly, thus it’s important to keep support for a dynamic list of filters), you can use the `getVoices`.

This method returns the full list of available filters as well as the currently selected one. The message’s payload looks like this:

```json
{
  "action": "getVoices",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```

As a result of this message, your application should be expecting an equally named event from the API: `getVoices` . 

The structure of the resulting message contains the list you’d expect:

```json
{
  "id": "100",
  "action": "getVoices",
  "payload": {
    "voices": [
      {
        "id": "2x1",
        "friendlyName": "2x1",
        "enabled": true,
        "isCustom": false,
        "favorited": false,
        "isNew": false,
        "bitmapChecksum": "177b8d4e8cf4b4a089b8ab70734e9e1d",
        "isPurchased": false
      },
      
	  //....
    ],
    "currentVoice": "claudia"
  },
  "actionType": "getVoices",
  "actionObject": {
    "voices": [
      {
        "id": "2x1",
        "friendlyName": "2x1",
        "enabled": true,
        "isCustom": false,
        "favorited": false,
        "isNew": false,
        "bitmapChecksum": "177b8d4e8cf4b4a089b8ab70734e9e1d",
        "isPurchased": false
      },
     
 //....  
    ],
    "currentVoice": "claudia"
  }
}
```

For loading one of these filters, all you need is the `id` of each one.

And to understand which one is currently selected, you can use the `currentVoice` property which holds the ID of the active voice filter.
