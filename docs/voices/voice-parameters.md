---
title: Voice filter parameters
---

# Voice filter parameters

Every voice filter has some configuration settings (known as **parameters**) that make it sound the way it does.
Using the Control API, you can retrieve and modify those parameters.

The key to remember here is that every voice filter has a distinct set of parameters and if you want to provide a UI to change them, 
 that UI needs to be able to adapt to the dynamic nature of each voice filter.

To start, you need to do retrieve the actual parameters of the currently selected voice filter, which you can do through the `getCurrentVoice` message. 
Once you have the parameters, you can affect them by changing their internal configuration with the `setCurrentVoiceParameter` message.

## Getting the current voice filter

To get the currenly selected voice filter and its list of paramters, use the following message:

```json
{
  "action": "getCurrentVoice",
  "id": "[unique ID of your message]",
  "payload": {}
}
``` 

This message will trigger the `getCurrentVoice` event from the server, and the payload of that event will look like this:

```json
{
  "actionType": "getCurrentVoice",
  "actionObject": {
    "voiceID": "nofx",
    "parameters": {
      "_Master Volume": {
        "name": "_Master Volume",
        "default": 1,
        "maxValue": 2,
        "minValue": 0,
        "displayNormalized": true,
        "typeController": 0,
        "value": 1
      },
      "_Reduce Background Noise": {
        "name": "_Reduce Background Noise",
        "default": -28,
        "maxValue": 0,
        "minValue": -40,
        "displayNormalized": true,
        "typeController": 0,
        "value": -28
      },
      "bass": {
        "name": "Bass",
        "default": 0,
        "maxValue": 5,
        "minValue": -5,
        "displayNormalized": true,
        "typeController": 0,
        "value": 0
      },
      "mid": {
        "name": "Mid",
        "default": 0,
        "maxValue": 5,
        "minValue": -5,
        "displayNormalized": true,
        "typeController": 0,
        "value": 0
      },
      "treble": {
        "name": "Treble",
        "default": 0,
        "maxValue": 5,
        "minValue": -5,
        "displayNormalized": true,
        "typeController": 0,
        "value": 0
      },
      "intelligible": {
        "name": "Intelligible",
        "default": 13000,
        "maxValue": 15000,
        "minValue": 1000,
        "displayNormalized": true,
        "typeController": 0,
        "value": 13000
      },
      "mix": {
        "name": "Mix",
        "default": 1,
        "maxValue": 1,
        "minValue": 0,
        "displayNormalized": true,
        "typeController": 0,
        "value": 1
      },
      "voiceVolume": {
        "name": "VoiceVolume",
        "default": 7.7,
        "maxValue": 10,
        "minValue": 0,
        "displayNormalized": true,
        "typeController": 0,
        "value": 7.7
      }
    }
  }
}
```

Keep in mind this is the structure you'd get if there is no voice filter selected (notice the `nofx` value on the `voiceID` property).
For other filters, you'd be getting different parameters, but the basic structure of the payload is:

```json
{
  "actionType": "getCurrentVoice",
  "actionObject": {
    "voiceID": "nofx",
    "parameters": [ 
        //....
    ] 
}
```

## Changing parameters on the current voice filter
To change the configuration of the active filter, you'll have to go parameter by parameter and send a `setCurrentVoiceParameter` message for each and every parameter you want to change.

The structure of that message looks like this:

```json
{
  "action": "setCurrentVoiceParameter",
  "id": "[unique ID for your message]",
  "payload": {
    "parameterName": "[name of the parameter you want to change]",
    "parameterValue": {
      "value": [an object containing the list of attributes and their values for this parameter]
    }
  }
}
```

To put this into perspective, let's revisit the example list of parameters for the `nofx` filter, which you can also modify. 
Suppose you want to change the "default volume", we'd then go to the list of parameters and look for the right one. 
Then write the following JSON:

```json
{
  "action": "setCurrentVoiceParameter",
  "id": "123456",
  "payload": {
    "parameterName": "VoiceVolume",
    "parameterValue": {
      "value": {
        "default": 7, 
        "maxValue": 10,
        "minValue": 0,
        "displayNormalized": true,
        "typeController": 0,
        // highlight-next-line
        "value": 9 //we changed this line
      }
    }
  }
}
```

With this change, you should start hearing your own voice louder than it was a second ago.
In fact, if you're looking at the Voicemod app, you should see the parameter changed in real-time as well:

![Voice volume updated on the App](../img/voice-volume-changed.png)

As a result of this message, the server will trigger a `setCurrentVoiceParameter` event that will contain the list of all parameters for the currently active voice (including the ones you manually changed before).

```json
{
  "voiceId": "nofx",
  "parameters": {
    "_Master Volume": {
      "name": "_Master Volume",
      "default": 1,
      "maxValue": 2,
      "minValue": 0,
      "displayNormalized": true,
      "typeController": 0,
      "value": 1
    },
    "_Reduce Background Noise": {
      "name": "_Reduce Background Noise",
      "default": -28,
      "maxValue": 0,
      "minValue": -40,
      "displayNormalized": true,
      "typeController": 0,
      "value": -28
    },
    "bass": {
      "name": "Bass",
      "default": 0,
      "maxValue": 5,
      "minValue": -5,
      "displayNormalized": true,
      "typeController": 0,
      "value": 0
    },
    "mid": {
      "name": "Mid",
      "default": 0,
      "maxValue": 5,
      "minValue": -5,
      "displayNormalized": true,
      "typeController": 0,
      "value": 0
    },
    "treble": {
      "name": "Treble",
      "default": 0,
      "maxValue": 5,
      "minValue": -5,
      "displayNormalized": true,
      "typeController": 0,
      "value": 0
    },
    "intelligible": {
      "name": "Intelligible",
      "default": 13000,
      "maxValue": 15000,
      "minValue": 1000,
      "displayNormalized": true,
      "typeController": 0,
      "value": 13000
    },
    "mix": {
      "name": "Mix",
      "default": 1,
      "maxValue": 1,
      "minValue": 0,
      "displayNormalized": true,
      "typeController": 0,
      "value": 1
    },
    "voiceVolume": {
      "name": "VoiceVolume",
      "default": 7.7,
      "maxValue": 10,
      "minValue": 0,
      "displayNormalized": true,
      "typeController": 0,
      // highlight-next-line
      "value": 9 //this is the one we changed!
    }
  }
}
```

## Reacting the changes on the UI
Even if we're not the ones updating those values, the server will issue an event if the user is manually adjusting the voice parameters.
You can decide to react to those changes however you see fit, or ignore them altogether.

The event received from the server is called `voiceParameterUpdated` and it looks like this:

```json
{
  "payload": {
    "voiceId": "[the ID of the voice being affected]",
    "parameter": {
      "source": "Websocket",
      "name": "[the name of the parameter]",
      "value": [the new value]
    }
  },
  "action": "voiceParameterUpdated",
  "id": "",
  "actionType": "voiceParameterUpdated",
  "actionObject": {
    "voiceId": "[the name of the parameter]",
    "parameter": {
      "Source": "Websocket",
      "Name":"[the name of the parameter]",
      "Value": [the new value]
    }
  }
}
```

Notice how here we're only getting the current value, from the UI users can't really affect other things like the `maxValue`.
So for example, if the user decided to change the voice volume manually, we'd get something like this:

```json
{
  "payload": {
    "voiceId": "nofx",
    "parameter": {
      "source": "Websocket",
      "name": "voiceVolume",
      "value": 9
    }
  },
  "action": "voiceParameterUpdated",
  "id": "",
  "actionType": "voiceParameterUpdated",
  "actionObject": {
    "voiceId": "nofx",
    "parameter": {
      "Source": "Websocket",
      "Name": "voiceVolume",
      "Value": 9
    }
  }
}
```
