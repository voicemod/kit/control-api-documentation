---
title: What is a voice filter?
---

# What is a voice filter?

Voice filters are the bread and butter of almost anything you’d do with the Control API. 

That said, Voice filters are essentially real-time effects that will affect the way your voice sounds through the microphone.

There are a few of considerations to have with regards to these filters:

- You can only have one filter active at the time. Whenever you load a voice filter, the previously active one (if any) will be unloaded.
- Voice filters last until otherwise stated by you. You can stop a filter either by using the `nofx` voice ID or by loading a new voice filter.
- To enable the effects you set through the API, you must have enabled the “Voice Changer” switch on the actual Voicemod App.

## Structure of a voice filter

While a “voice filter” is a real-time effect you put on your own voice, each filter in our system has a distinct data structure with the following properties  (returned by the `getVoices` method ):

- **id** (`string`): The unique identifier of the filter that can be used in method such as `loadVoice` to activate a voice filter.
- **friendlyName** (`string`): A detailed name that can be used to present more information to the user.
- **enabled** (`boolean`):  It indicates whether or not this filter can be used by the application. If the filter is “disabled” (i.e this value is `false`) the filter itself will be listed, but the application won’t be able to use it.
- **isCustom** (`boolean`): This value will be `true` if the voice filter was created using VoiceLabs.
- **favorited** (`boolean`): Indicates whether or not this voice filter is marked as one of the user’s favorites in the desktop application.
- **isNew** (`boolean`): This flag can be used to understand if the “new” label should be shown next to this filter on the UI.
- **bitmapChecksum** (`string - hash`): Used to verify the integrity of the bitmap image of each voice filter (bitmaps are downloaded separately ,so this hash can be used to peform the check).
- **isPurchased** (`boolean`): This flag indicates whether the user purchased this voice filter on the store.





