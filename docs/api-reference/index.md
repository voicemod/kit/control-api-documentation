# Voicemod Control API 1.0.0 documentation

* License: [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)


<h2>Known Issues</h2> <div>• The <b>actionID</b> field is null in some inbound messages</div> <div>• A <b>getBitmap</b> message will fail if both payload fields (voiceID and memeId) are included</div>
<h2>Contact</h2> Feel free to get in touch with us at <a href="mailto:devservices@voicemod.net">devservices@voicemod.net</a>.

## Servers

### `voicemod` Server

* URL: `ws://localhost:59129/v1/`
* Protocol: `ws`

Voicemod App (Websocket Server)


## Operations

### PUB `registerClient` Operation

<b>⚠This is the first message that must be sent to the Voicemod app</b><br/> Once a connection is established, the plugin must send this message to make the app server aware of its presence. This is a prerequisite for communication between the plugin and app to be enabled.

#### Message `RegisterClientAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"registerClient"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.clientKey | string | The unique identifier for the partner. This key should be provided by Voicemod | - | - | - |

> Examples of payload _(generated)_

```json
{
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "action": "registerClient",
  "payload": {
    "clientKey": "MyUniqueClientKey"
  }
}
```



### SUB `registerClient` Operation

<b>⚠This is the response to the first message that must be sent to the Voicemod app</b><br/> This message is triggered in response to a <a href="#operation-publish-registerClient">registerClient</a> message. A <b>200, Authorized</b> response is required for communication between the plugin and app to be enabled.

#### Message `RegisterClientTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"registerClient"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.status | - | The result of a registerClient request. Should be '200, "authorized"' if authorized, otherwise, '401, "unauthorized"' | - | - | **additional properties are allowed** |
| payload.status.code | number | Status code that complies with HTTP response standard | - | - | - |
| payload.status.description | string | Description of status code | - | - | - |

> Examples of payload _(generated)_

```json
{
  "action": "registerClient",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "status": {
      "code": 200,
      "description": "Authorized"
    }
  }
}
```



### PUB `getUser` Operation

Requests the id of the current user.

#### Message `GetUserAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getUser"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getUser",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `getUser` Operation

This message is triggered in response to a <a href="#operation-publish-getUser">getUser</a> message.

#### Message `GetUserTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getUser"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.userId | string | The user's id in the Voicemod app | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "getUser",
  "actionObject": {
    "userId": "a5a564eb-4976-4b4e-a660-5adcc855560e"
  }
}
```



### PUB `getUserLicense` Operation

Requests user license information.

#### Message `GetUserLicenseAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getUserLicense"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getUserLicense",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `getUserLicense` Operation

This message is triggered as a response to a <a href="#operation-publish-getUserLicense">getUserLicense</a> message.

#### Message `GetUserLicenseTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getUserLicense"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.licenseType | string | The user's license type in the Voicemod app | allowed (`"pro"`, `"free"`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "actionType": "getUserLicense",
  "actionObject": {
    "licenseType": "pro"
  }
}
```



### SUB `licenseTypeChanged` Operation

This message is triggered when the user license has changed in the Voicemod app.

#### Message `LicenseTypeChangedTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"licenseTypeChanged"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.licenseType | string | The user's license type in the Voicemod app | allowed (`"pro"`, `"free"`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "actionType": "licenseTypeChanged",
  "appVersion": "2.7.0.3",
  "actionID": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "licenseType": "free"
  },
  "context": ""
}
```



### PUB `getRotatoryVoicesRemainingTime` Operation

Requests the remaining time (in seconds) before a refresh is made to the selection of voices that are available under the free version.

#### Message `GetRotatoryVoicesRemainingTimeAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getRotatoryVoicesRemainingTime"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getRotatoryVoicesRemainingTime",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `getRotatoryVoicesRemainingTime` Operation

This message is triggered in response to a <a href="#operation-publish-getRotatoryVoicesRemainingTime">getRotatoryVoicesRemainingTime</a> message.

#### Message `GetRotatoryVoicesRemainingTimeTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getRotatoryVoicesRemainingTime"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.remainingTime | number | The remaining time (in seconds) before a refresh is made to the selection of voices that are available under the free version | - | format (`long`) | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "getRotatoryVoicesRemainingTime",
  "actionObject": {
    "remainingTime": 71769
  }
}
```



### PUB `getVoices` Operation

Requests the <b>list of the available voices</b> and the <b>current voice selected</b> in the app for the current user.

#### Message `GetVoicesAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getVoices"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getVoices",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `getVoices` Operation

This message is triggered in response to a change in available voices (a change from free user to pro user or an update in daily free voices), and in response to a <a href="#operation-publish-getVoices">getVoices</a> message.

#### Message `GetVoicesTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getVoices"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.voices | array  | - | - | - | - |
| actionObject.voices.id | string | The id of the voice | - | - | - |
| actionObject.voices.friendlyName | string | The friendly name of the voice | - | - | - |
| actionObject.voices.enabled | boolean | True if enabled for the user | - | - | - |
| actionObject.voices.favorited | boolean | True if the user has marked the voice as favorite | - | - | - |
| actionObject.voices.isNew | boolean | True if the voice is new | - | - | - |
| actionObject.voices.isCustom | boolean | True if the voice was generated by the user | - | - | - |
| actionObject.voices.bitmapChecksum | string | The bitmap checksum enables the client to persist or cache the image to avoid excess getBitmap requests and improve performance | - | - | - |
| actionObject.currentVoice | string | The currently selected voice in the app | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "getVoices",
  "actionObject": {
    "voices": [
      {
        "id": "baby",
        "friendlyName": "Baby",
        "enabled": true,
        "isCustom": false,
        "favorited": false,
        "isNew": false,
        "bitmapChecksum": "9af56b7cb7a87db7b9d4e93852599981"
      },
      {
        "id": "blocks",
        "friendlyName": "Blocks",
        "enabled": true,
        "isCustom": false,
        "favorited": false,
        "isNew": false,
        "bitmapChecksum": "446244ad8e06e437ed13fa723d1df019"
      }
    ]
  },
  "currentVoice": "nofx"
}
```



### PUB `getCurrentVoice` Operation

Requests the voice that is currently selected in the app.

#### Message `GetCurrentVoiceAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getCurrentVoice"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getCurrentVoice",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `getCurrentVoice` Operation

This message is triggered in response to a <a href="#operation-publish-getCurrentVoice">getCurrentVoice</a> message.

#### Message `GetCurrentVoiceTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getCurrentVoice"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.voiceID | string | The id of the current voice | - | - | - |
| actionObject.Parameters | object | Property names and values for the current voice | - | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "actionType": "getCurrentVoice",
  "actionObject": {
    "voiceID": "nofx",
    "parameters": [
      {
        "_Master Volume": {
          "default": 1,
          "maxValue": 2,
          "minValue": 0,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      },
      {
        "voiceVolume": {
          "default": 6.5,
          "maxValue": 6.5,
          "minValue": 0,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      },
      {
        "_Reduce Background Noise": {
          "default": -28,
          "maxValue": 0,
          "minValue": -40,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      },
      {
        "noise Reduction": {
          "default": 0,
          "maxValue": 100,
          "minValue": 0,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      }
    ]
  }
}
```



### PUB `getAllSoundboard` Operation

Requests all soundboards from the app with info for each soundboard and each sound that is enabled based on the current user's license.

#### Message `GetAllSoundboardAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getAllSoundboard"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getAllSoundboard",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `getAllSoundboard` Operation

This message is triggered in response to a change in a soundboard (a change from free user to pro user), and in response to a <a href="#operation-publish-getAllSoundboard">getAllSoundboard</a> message.

#### Message `GetAllSoundboardTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getAllSoundboard"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.soundboards | array  | - | - | - | - |
| actionObject.soundboards.Id | string | The id of the soundboard profile | - | - | - |
| actionObject.soundboards.Name | string | The friendly name of the soundboard | - | - | - |
| actionObject.soundboards.IsCustom | boolean | True if the soundboard profile was created by the user | - | - | - |
| actionObject.soundboards.Enabled | boolean | True if user can execute meme sounds in this soundboard | - | - | - |
| actionObject.soundboards.ShowProLogo | boolean | True if the user does not have a pro license and the soundboard is not enabled | - | - | - |
| actionObject.soundboards.sounds | array  | The meme sounds contained in this soundboard profile | - | - | - |
| actionObject.soundboards.sounds.Id | string | The id of the meme sound | - | - | - |
| actionObject.soundboards.sounds.Name | string | The friendly name of the meme sound | - | - | - |
| actionObject.soundboards.sounds.IsCustom | boolean | True if the meme sound was created by the user | - | - | - |
| actionObject.soundboards.sounds.Enabled | boolean | True if user can execute this meme sound | - | - | - |
| actionObject.soundboards.sounds.ShowProLogo | boolean | True if the user does not have a pro license and the meme sound is not enabled | - | - | - |
| actionObject.soundboards.sounds.playbackMode | string | The playback mode for the meme sound - <b>PlayRestart</b>&#58; On hit, stop the sound if it is still playing, then restart it from the beginning. - <b>PlayStop</b>&#58; On hit, stop the sound if it was still playing, restart it otherwise. - <b>PlayPause</b>&#58; On hit, pause the sound if it was still playing, restart it otherwise. - <b>PlayOverlap</b>&#58; On hit, reproduce the sound without stopping any other instances still playing. - <b>PlayLoopOnPress</b>&#58; OnKeyDown, reproduce the sound on a loop and don't stop until the OnKeyUp. | allowed (`"PlayRestart"`, `"PlayPause"`, `"PlayStop"`, `"PlayOverlap"`, `"PlayLoopOnPress"`) | - | **additional properties are allowed** |
| actionObject.soundboards.sounds.loop | boolean | True if the meme sound should be played back in loop | - | - | - |
| actionObject.soundboards.sounds.muteOtherSounds | boolean | True if the meme sound should stop playback of other sounds | - | - | - |
| actionObject.soundboards.sounds.muteVoice | boolean | True if the meme sound should mute the user's microphone | - | - | - |
| actionObject.soundboards.sounds.bitmapChecksum | string | The bitmap checksum enables the client to persist or cache the image to avoid excess getBitmap requests and improve performance | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "getAllSoundboard",
  "actionObject": {
    "soundboards": [
      {
        "id": "75bbb8c1-7dd9-434f-8d44-c99b4f1c61df",
        "name": "My Soundboard",
        "isCustom\"": true,
        "enabled\"": true,
        "showProLogo\"": false,
        "sounds": [
          {
            "id": "ad6efa6b-4872-4217-9465-5415178dfc1f",
            "name": "Nuke Alert",
            "isCustom": false,
            "enabled": true,
            "playbackMode": "PlayRestart",
            "loop": false,
            "muteOtherSounds": false,
            "muteVoice": false,
            "stopOtherSounds": true,
            "showProLogo": false,
            "bitmapChecksum": "14-04-DB-12-50-09-25-14-18-C5-62-2D-4A-AB-8D-31"
          }
        ]
      },
      {
        "id": "96a38ce2-f6b6-4cb2-ada6-df41f845991a",
        "name": "Anime",
        "isCustom\"": false,
        "enabled\"": false,
        "showProLogo\"": true,
        "sounds": [
          {
            "id": "1b7cda07-409f-4c25-999c-4729e8a931a3",
            "name": "Temple Gong",
            "isCustom": false,
            "enabled": false,
            "playbackMode": "PlayRestart",
            "loop": false,
            "muteOtherSounds": false,
            "muteVoice": false,
            "stopOtherSounds": true,
            "showProLogo": false,
            "bitmapChecksum": "2C-A0-DD-13-B5-93-F1-C4-4D-80-82-A5-74-DE-11-1F"
          },
          {
            "id": "b3ddaeea-8001-4e87-a7e6-362db6e96f2a",
            "name": "Punch",
            "isCustom": false,
            "enabled": false,
            "playbackMode": "PlayRestart",
            "loop": false,
            "muteOtherSounds": false,
            "muteVoice": false,
            "stopOtherSounds": true,
            "showProLogo": false,
            "bitmapChecksum": "D1-FA-F1-2B-D9-44-88-21-06-C9-02-05-C2-D8-D7-E8"
          }
        ]
      }
    ]
  }
}
```



### SUB `getActiveSoundboardProfile` Operation

This message is triggered in response to a change in of a currently active soundboard.

#### Message `GetActiveSoundboardProfile`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| type | string | - | const (`"getActiveSoundboardProfile"`) | - | **additional properties are allowed** |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.profileId | string | - | - | - | - |

> Examples of payload _(generated)_

```json
{
  "type": "getActiveSoundboardProfile",
  "payload": {
    "profileId": "string"
  }
}
```



### PUB `getMemes` Operation

Requests the list of the available meme sounds for the current user.

#### Message `GetMemesAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getMemes"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getMemes",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `getMemes` Operation

This message is triggered in response to a change in available meme sounds (the user has added or removed a meme), and in response to a <a href="#operation-publish-getMemes">getMemes</a> message.

#### Message `GetMemesTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getMemes"`) | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.listOfMemes | array  | - | - | - | - |
| actionObject.listOfMemes.Name | string | The friendly name of the meme sound | - | - | - |
| actionObject.listOfMemes.Filename | string | The filename of the meme sound | - | - | - |
| actionObject.listOfMemes.Type | string | The playback mode for the meme sound - <b>PlayRestart</b>&#58; On hit, stop the sound if it is still playing, then restart it from the beginning. - <b>PlayStop</b>&#58; On hit, stop the sound if it was still playing, restart it otherwise. - <b>PlayPause</b>&#58; On hit, pause the sound if it was still playing, restart it otherwise. - <b>PlayOverlap</b>&#58; On hit, reproduce the sound without stopping any other instances still playing. - <b>PlayLoopOnPress</b>&#58; OnKeyDown, reproduce the sound on a loop and don't stop until the OnKeyUp. | allowed (`"PlayRestart"`, `"PlayPause"`, `"PlayStop"`, `"PlayOverlap"`, `"PlayLoopOnPress"`) | - | **additional properties are allowed** |
| actionObject.listOfMemes.Image | string | The filename of the meme sound's image | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "getMemes",
  "appVersion": "2.0.0.34",
  "actionId": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "listOfMemes": [
      {
        "Name": "My Soundboard - Toc toc Boom",
        "FileName": "78",
        "Type": "PlayRestart",
        "Image": "iVBORw0KUuxZaGehlmhmkW50Ng9dM [...] =="
      },
      {
        "Name": "My Soundboard - Jungle King",
        "FileName": "79",
        "Type": "PlayRestart",
        "Image": "iVBORw0KUuxZaGehlmhmkW50Ng9dM [...] =="
      },
      {
        "Name": "My Soundboard - Evil laugh",
        "FileName": "80",
        "Type": "PlayRestart",
        "Image": "iVBORw0KUuxZaGehlmhmkW50Ng9dM [...] =="
      }
    ]
  },
  "context": ""
}
```



### PUB `getBitmap` Operation

Requests the icon for a given <b>voice</b> or <b>meme</b>.

#### Message `GetBitmapAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | examples (`{"action":"getBitmap","id":"ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6","payload":{"voiceID":"nofx"}}`, `{"action":"getBitmap","id":"ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6","payload":{"memeId":"26"}}`) | - | **additional properties are allowed** |
| action | string | - | const (`"getBitmap"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | <b>Note</b> - only one of the two fields (voiceID or memeId) should be included in an outbound payload | - | - | **additional properties are allowed** |
| payload.voiceID | string | The ID of the voice we want to recover the bitmap for | - | - | - |
| payload.memeId | string | The ID of the meme we want to recover the bitmap for | - | - | - |

> Examples of payload

```json
{
  "action": "getBitmap",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "voiceID": "nofx"
  }
}
```


```json
{
  "action": "getBitmap",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "memeId": "26"
  }
}
```



### SUB `getBitmap` Operation

This message is triggered in response to a <a href="#operation-publish-getBitmap">getBitmap</a> message.

#### Message `GetBitmapTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"getBitmap"`) | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | oneOf | - | - | - | **additional properties are allowed** |
| actionObject.0 (oneOf item) | - | - | - | - | **additional properties are allowed** |
| actionObject.0.voiceID | string | The id of the selected voice | - | - | - |
| actionObject.0.result | - | - | - | - | **additional properties are allowed** |
| actionObject.0.result.default | string | The default icon for the resource (base64 encoded binary) | - | - | - |
| actionObject.0.result.selected | string | The icon for the resource when selected (base64 encoded binary) | - | - | - |
| actionObject.0.result.transparent | string | The icon with transparency for the resource | - | - | - |
| actionObject.1 (oneOf item) | - | - | - | - | **additional properties are allowed** |
| actionObject.1.memeId | string | The id of the selected meme sound | - | - | - |
| actionObject.1.result | - | - | - | - | **additional properties are allowed** |
| actionObject.1.result.image | string | The default icon for the resource (base64 encoded binary) | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "getBitmap",
  "actionID": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "appVersion": "2.0.0.34",
  "context": "",
  "actionObject": {
    "default": "iVBORw0KUuxZaGehlmhmkW50Ng9dM [...] ==",
    "selected": "iVBORw0KUuxZaGehlmhmkW50Ng9dM [...] ==",
    "transparent": "iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfN[...] =="
  }
}
```



### PUB `loadVoice` Operation

Requests a change to the user's selected voice.

#### Message `LoadVoiceAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"loadVoice"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.voiceID | string | The id of the voice we want to select | - | - | - |
| payload.parameterName | string | The name of a parameter to be changed when selecting the voice | - | - | - |
| payload.parameterValue | string | The value of the parameter to be changed | - | - | - |

> Examples of payload _(generated)_

```json
{
  "action": "loadVoice",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "voiceID": "cave"
  }
}
```



### PUB `selectRandomVoice` Operation

Requests a change to a randomly selected voice.

#### Message `SelectRandomVoiceAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | examples (`{"action":"selectRandomVoice","id":"ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6","payload":{}}`, `{"action":"selectRandomVoice","id":"ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6","payload":{"mode":"FavoriteVoices"}}`) | - | **additional properties are allowed** |
| action | string | - | const (`"selectRandomVoice"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.mode | string | Shuffle mode - can be one of the following&#58;<br/> - <b>AllVoices</b>&#58; Select any voice.  Available if the user has a Pro license<br/> - <b>FreeVoices</b>&#58; Select any voice available in the free version<br/> - <b>FavoriteVoices</b>&#58; Select only from favorited voices<br/> - <b>CustomVoices</b>&#58; Select only from voices created by the user | allowed (`"AllVoices"`, `"FreeVoices"`, `"FavoriteVoices"`, `"CustomVoices"`) | - | **additional properties are allowed** |

> Examples of payload

```json
{
  "action": "selectRandomVoice",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```


```json
{
  "action": "selectRandomVoice",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "mode": "FavoriteVoices"
  }
}
```



### SUB `voiceChangedEvent` Operation

This message is triggered in response to a change in the currently selected voice (through user interaction in the app), in response to a <a href="#operation-publish-selectVoice">selectVoice</a> message, and in response to a <a href="#operation-publish-getCurrentVoice">getCurrentVoice</a> message.

#### Message `VoiceChangedEventTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"voiceChangedEvent"`) | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.voiceID | string | The id of the current voice | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "voiceChangedEvent",
  "appVersion": "2.0.0.34",
  "actionId": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "voiceID": "deep"
  }
}
```



### PUB `getHearMyselfStatus` Operation

Requests a toggle of the "Hear my voice" button in the app.

#### Message `GetHearMyselfStatusAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getHearMyselfStatus"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getHearMyselfStatus",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### PUB `toggleHearMyVoice` Operation

Requests a toggle of the "Hear my voice" button in the app.

#### Message `ToggleHearMyVoiceAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"toggleHearMyVoice"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "toggleHearMyVoice",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `toggleHearMyVoice` Operation

This message is triggered in response to a user interaction with the "Hear My Voice" toggle in the app, in response to a <a href="#operation-publish-toggleHearMyVoice">toggleHearMyVoice</a> message, and in response to a <a href="#operation-publish-getHearMyselfStatus">getHearMyselfStatus</a> message.

#### Message `ToggleHearMyVoiceTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | any | - | - | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.value | boolean | The value (true or false) of the parameter | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "toggleHearMyVoice",
  "appVersion": "2.0.0.34",
  "actionId": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "value": true
  }
}
```



### PUB `getVoiceChangerStatus` Operation

Requests the current state of the "Voice Changer" button in the app.

#### Message `GetVoiceChangerStatusAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getVoiceChangerStatus"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getVoiceChangerStatus",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### PUB `toggleVoiceChanger` Operation

Requests a toggle of the "Voice Changer" button in the app.  As a response, a <a href="#sub-voicechangerenabledevent-operation">voiceChangerEnabledEvent</a> or a <a href="#sub-voicechangerdisabledevent-operation">voiceChangerDisabledEvent</a> is triggered (depending on the value of the toggle).

#### Message `ToggleVoiceChangerAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"toggleVoiceChanger"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "toggleVoiceChanger",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `toggleVoiceChanger` Operation

This message is triggered in response to a user interaction with the "Voice Changer" toggle in the app and in response to a <a href="#operation-publish-getVoiceChangerStatus">getVoiceChangerStatus</a> message.

#### Message `ToggleVoiceChangerTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"toggleVoiceChanger"`) | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.value | boolean | The value (true or false) of the parameter | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "toggleVoiceChanger",
  "appVersion": "2.0.0.34",
  "actionId": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "value": false
  }
}
```



### SUB `voiceChangerEnabledEvent` Operation

This message is triggered is response to a <a href="#pub-togglevoicechanger-operation">toggleVoiceChanger</a> message that ends up <b>enabling the voice changer</b>.


### SUB `voiceChangerDisabledEvent` Operation

This message is triggered is response to a <a href="#pub-togglevoicechanger-operation">toggleVoiceChanger</a> message that ends up <b>disabling the voice changer</b>.


### PUB `getBackgroundEffectStatus` Operation

Requests the current state of the "Background Effects" button in the app.

#### Message `GetBackgroundEffectStatusAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getBackgroundEffectStatus"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getBackgroundEffectStatus",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### PUB `toggleBackground` Operation

Requests a toggle of the "Background Effects" button in the app.

#### Message `ToggleBackgroundAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"toggleBackground"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "toggleBackground",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `toggleBackground` Operation

This message is triggered in response to a user interaction with the "Background Effects" toggle in the app, in response to a <a href="#operation-publish-toggleBackground">toggleBackground</a> message, and in response to a <a href="#operation-publish-getBackgroundEffectStatus">getBackgroundEffectStatus</a> message.

#### Message `ToggleBackgroundTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"toggleBackground"`) | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.value | boolean | The value (true or false) of the parameter | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "toggleBackground",
  "appVersion": "2.0.0.33",
  "actionId": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "value": false
  }
}
```



### PUB `getMuteMicStatus` Operation

Requests the current state of the "Mute" button in the app.

#### Message `GetMuteMicStatusAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getMuteMicStatus"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getMuteMicStatus",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### PUB `toggleMuteMic` Operation

Requests a toggle of the "Mute" button in the app.

#### Message `ToggleMuteAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"toggleMuteMic"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "toggleMuteMic",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `toggleMuteMic` Operation

This message is triggered in response to a user interacting with the "Mute" toggle in the app, in response to a <a href="#operation-publish-toggleMuteMic">toggleMuteMic</a> message, and in response to a <a href="#operation-publish-getMuteMicStatus">getMuteMicStatus</a> message.

#### Message `ToggleMuteTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"toggleMute"`) | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.value | boolean | The value (true or false) of the parameter | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "toggleMuteMic",
  "appVersion": "2.0.0.33",
  "actionId": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "value": true
  }
}
```



### PUB `setBeepSound` Operation

Requests a state change of the "beep" sound that is normally actioned by the user to censor something he or she is saying.

#### Message `SetBeepSoundAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"setBeepSound"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.badLanguage | number | New setting&#58;<br/> - 1 if we want to start the beep<br/> - 0 if we want to end it | - | format (`int`) | - |

> Examples of payload _(generated)_

```json
{
  "action": "setBeepSound",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "badLanguage": 1
  }
}
```



### PUB `playMeme` Operation

Requests playback of a meme sound.

#### Message `PlayMemeAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"playMeme"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.FileName | string | The file name of the sound we want to play | - | - | - |
| payload.IsKeyDown | boolean | True if sending a KeyDown action | - | - | - |

> Examples of payload _(generated)_

```json
{
  "action": "playMeme",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "FileName": "80",
    "IsKeyDown": true
  }
}
```



### PUB `stopAllMemeSounds` Operation

Requests playback stop of all meme sounds currently playing.

#### Message `StopAllMemeSoundsAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"stopAllMemeSounds"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "stopAllMemeSounds",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### PUB `getMuteMemeForMeStatus` Operation

Requests the current status of the "Mute for me" button in the app (Soundboard menu).

#### Message `GetMuteMemeForMeStatusAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"getMuteMemeForMeStatus"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "getMuteMemeForMeStatus",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### PUB `toggleMuteMemeForMe` Operation

Requests a toggle of the "Mute for me" button in the app (Soundboard menu).

#### Message `ToggleMuteForMeMemeAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"toggleMuteMemeForMe"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | object | - | const (`{}`) | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "action": "toggleMuteMemeForMe",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {}
}
```



### SUB `toggleMuteMemeForMe` Operation

This message is triggered in response to a change in "Mute for me" (through user interaction in the app), in response to a <a href="#operation-publish-toggleMuteMemeForMe">toggleMuteMemeForMe</a> message, and in response to a <a href="#operation-publish-getMuteMemeForMeStatus">getMuteMemeForMeStatus</a> message.

#### Message `ToggleMuteForMeMemeTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"toggleMuteMemeForMe"`) | - | **additional properties are allowed** |
| actionID | string | - | - | - | - |
| appVersion | string | - | - | - | - |
| context | object | - | - | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.value | boolean | The value (true or false) of the parameter | - | - | - |

> Examples of payload _(generated)_

```json
{
  "actionType": "toggleMuteMemeForMe",
  "appVersion": "2.0.0.33",
  "actionId": "9aabf036-0ba4-4359-a2d6-90ac8691e320",
  "actionObject": {
    "value": true
  }
}
```



### PUB `setCurrentVoiceParameter` Operation

Requests a change of parameter for currently selected voice.

#### Message `SetCurrentVoiceParameterAction`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| action | string | - | const (`"setCurrentVoiceParameter"`) | - | **additional properties are allowed** |
| id | string | - | - | - | - |
| payload | - | - | - | - | **additional properties are allowed** |
| payload.parameterName | string | Name of voice parameter | - | - | - |
| payload.parameterValue | - | - | - | - | **additional properties are allowed** |
| payload.parameterValue.maxValue | number | maxValue | - | - | - |
| payload.parameterValue.minValue | number | minValue | - | - | - |
| payload.parameterValue.displayNormalized | boolean | displayNormalized | - | - | - |
| payload.parameterValue.value | number | value | - | - | - |

> Examples of payload _(generated)_

```json
{
  "action": "setCurrentVoiceParameter",
  "id": "ff7d7f15-0cbf-4c44-bc31-b56e0a6c9fa6",
  "payload": {
    "parameterName": "Mix",
    "parameterValue": {
      "value": 0.6
    }
  }
}
```



### SUB `setCurrentVoiceParameter` Operation

This message is triggered in response to a change in "setCurrentVoiceParameter", in response to a <a href="#operation-publish-setCurrentVoiceParameter">setCurrentVoiceParameter</a> message.

#### Message `SetCurrentVoiceParameterTrigger`

##### Payload

| Name | Type | Description | Value | Constraints | Notes |
|---|---|---|---|---|---|
| (root) | - | - | - | - | **additional properties are allowed** |
| actionType | string | - | const (`"setCurrentVoiceParameter"`) | - | **additional properties are allowed** |
| actionObject | - | - | - | - | **additional properties are allowed** |
| actionObject.voiceID | string | The id of the current voice | - | - | - |
| actionObject.Parameters | object | Property names and values for the current voice | - | - | **additional properties are allowed** |

> Examples of payload _(generated)_

```json
{
  "actionType": "setCurrentVoiceParameter",
  "actionObject": {
    "voiceID": "nofx",
    "parameters": [
      {
        "_Master Volume": {
          "default": 1,
          "maxValue": 2,
          "minValue": 0,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      },
      {
        "voiceVolume": {
          "default": 6.5,
          "maxValue": 6.5,
          "minValue": 0,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      },
      {
        "_Reduce Background Noise": {
          "default": -28,
          "maxValue": 0,
          "minValue": -40,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      },
      {
        "noise Reduction": {
          "default": 0,
          "maxValue": 100,
          "minValue": 0,
          "displayNormalized": true,
          "typeController": 0,
          "value": 0
        }
      }
    ]
  }
}
```



