---
title:  Controlling the Application through the API
---

# Controlling the Application through the API

Other than Voice filters and Sounds, the Voicemod application allows you to control other aspects of the experience.

In this section we’ll cover the messages that affect the application directly and the user experience.

## Toggle the 3 main switches

The Voicemod app has 3 main switches that allow you to do everything else listed in this documentation. Those switches are:

- Hear myself
- Voice changer
- Background effects

You can of course toggle them manually on the lower left corner of the application. However, if you’re interested in understanding how to affect those switches through code, keep readin.

### Toggle “Hear myself”

When changing the way you sound it’s very useful to hear yourself to understand if the voice filter you selected is the one you want. 

Through this message you can turn this feature on and off on the application. Keep in mind you can also change it manually by clicking on the app itself.

![Switch enabling Hear Myself](./img/hear-myself-switch.png)

The message has the following format:

```jsx
{
  "action": "toggleHearMyVoice",
  "id": "[unique id for your message]",
  "payload": {}
}
```

As a response, the server will trigger a `hearMySelfDisabledEvent` or a `hearMySelfEnabledEvent` event depending on the new state of the switch.

You can listen to either of those events if you have related logic or a UI to update. In both events, there is no associated extra data, the name of the event itself tells you all you need to know about it.

#### Getting the initial state of the switch

You can also use the `getHearMyselfStatus` message to understand what’s the current state of that switch. This is specially useful during the start-up process of your app.

The message looks like this:

```jsx
{
  "action": "getHearMyselfStatus",
  "id": "[unique message id]",
  "payload": {}
}
```

As a result, the server will trigger a `toggleHearMyVoice` event, and inside it, you’ll find the information you need:

```jsx
{
  "actionType": "toggleHearMyVoice",
  "appVersion": "2.0.0.34",
  "actionId": "[the unique ID used when issuing the original message]",
  "actionObject": {
    "value": [boolean value indicating the status of the switch]
  }
}
```

The relevant piece of information here is the content of the `value` property. If it’s `true` then the switch is turned on. Otherwise it’ll be set to `false` and you won’t be hearing yourself until that changes.

### Toggle the voice changer feature

To achieve any of the actions detailed under the Voices section of this documentation, you must have the voice changer enabled.

You can do this manually by enabling this feature on the lower left corner of the application, like shown on the following image:

![Switch enabling voice changer](./img/voice-changer-switch.png)

Or you can toggle the switch on and off through the following command:

```jsx
{
  "action": "toggleVoiceChanger",
  "id": "[unique ID for the message]",
  "payload": {}
}
```

As a result, the server will issue either one of these events (depending on the final state of the toggle):

- A `voiceChangerEnabledEvent` event, when the toggle is **enabled**.
- A `voiceChangerDisabledEvent` event, when the toggle is **disabled**.

Here is what the **enable** state looks like:
```json 
{
  "actionType": "voiceChangerEnabledEvent",
  "appVersion": "2.0.0.34",
  "actionId": "[unique ID used for the original message]",
  "actionObject": {
  }
}
```
Here is what the **disabled** state looks like:
```json 
{
  "actionType": "voiceChangerDisabledEvent",
  "appVersion": "2.0.0.34",
  "actionId": "[unique ID used for the original message]",
  "actionObject": {
  }
}
```
#### What happens if the user toggles the voice changer manually from the app?

In that case, the API will trigger a `toggleVoiceChanger` event, and as the payload of that event, you'll get the current state of the toggle.


Here is what this message looks like
```json 
{
  "actionType": "voiceChangerDisabledEvent",
  "appVersion": "2.0.0.34",
  "actionId": "[unique ID used for the original message]",
  "actionObject": {
    "value": false|true 
  }
}
```
As you’ve probably guessed it, the value that you pretty much care about is the content of the `value` property under `actionObject` .
If it's `true` then the voice changer is active, otherwise it'll be disabled.

### Toggle background effects

Sometimes voice filters will also have background effects such as music playing while you speak, other sounds or even far distant voices. These are “background effects”, and you can actually decide whether or not they’ll been heard.

:::tip

To enable this switch, you have to select a voice filter that actually provides a background effect, otherwise you won’t be able to affect this switch.

:::

You can manually affect this switch by clicking on it on the lower left corner of the application:

![Background effects switch](./img/background-effects-switch.png)

You can also affect it by sending a `toggleBackground` message to the server.

The message looks like this:

```jsx
{
  "action": "toggleBackground",
  "id": "[unique ID for your message]",
  "payload": {}
}
```

As a response, you’ll get an event called `toggleBackground` triggered by the server:

```jsx
{
  "actionType": "toggleBackground",
  "appVersion": "2.0.0.33",
  "actionId": "[the unique ID you used for the original message]",
  "actionObject": {
    "value": false
  }
}
```

#### Getting the initial state of the switch

You can also get the initial state of the switch to understand what to do when your client application starts.

To do this, you have to send the `getBackgroundEffectStatus` message:

```jsx
{
  "action": "getBackgroundEffectStatus",
  "id": "[unique id for your messagge]",
  "payload": {}
}
```

As a response, the server will trigger a new `toggleBackground` event, like when send the `toggleBackground` message. 

```jsx
{
  "actionType": "toggleBackground",
  "appVersion": "2.0.0.33",
  "actionId": "[the unique ID you used for your 'getBackgroundEffectStatus' message]",
  "actionObject": {
    "value": [boolean indicating the current status of the switch]
  }
}
```

## Bleep yourself

The Voicemod application allows you to Bleep yourself through a keybinding, however, you can do so through the Control API as well. 

:::tip

By bleeping yourself, others can’t hear what you say, no matter what voice filter you may have active.

:::

To bleep yourself, you’ll have to send the `setBeepSound` message twice, once to enable the bleeping sound and one to disable it (remember this second one, otherwise the bleeping won’t stop).

To enable the sound, send:

```jsx
{
  "action": "setBeepSound",
  "id": "[unique ID for your message]",
  "payload": {
    "badLanguage": 1
  }
}
```

The key of the message is the “1” value for the `badLanguage` property.

To stop the sound, simply send the same message but with a “0”  instead.

```jsx
{
  "action": "setBeepSound",
  "id": "[unique ID for your message]",
  "payload": {
    "badLanguage": 0
  }
}
```