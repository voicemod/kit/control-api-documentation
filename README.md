# Control API Documentation

Welcome to the repository of the documentation site for the Control API ([https://control-api.voicemod.net](https://control-api.voicemod.net)).

Note that this website was built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator, so the following are the generic instructions to build the site:

## Installation & local development

Make sure to have Node 16+ installed on your system, and run:

```
$ npm install
```

### Local Development
To start a development server simply type:

```
$ npm start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

#### Algolia search
If you're looking to get the search working, you'll have to set-up the following environment variables inside an `.env` file in the root of the project:

```
PUBLIC_ALGOLIA_APP_ID=....
ALGOLIA_ADMIN_SECRET=....
PUBLIC_ALGOLIA_INDEX=....
ALGOLIA_SEARCH_KEY=....
```

Keep in mind that if you don't do this, the search bar won't work. You don't need a payed account on Algolia for this to work, with a free version all pages can be indexed.

## Contributing
Thank you for investing your time in contributing to our project! Any contribution you make will be reflected on [https://control-api.voicemod.net](https://control-api.voicemod.net) ✨.

In this guide you will get an overview of the contribution workflow from opening an issue, creating a Merge Request, reviewing, and merging the MR.

### How to contribute
There are a couple of ways to contribute to our documentation, mainly if you spot a problem with it (that includes typos, incorrect facts, or anything that can cause a misunderstanding of the guides), potential improvements you think would help the rest of the developer community.

When that happens, start by creating an **issue** describing your changes and the motivation for them.

#### Creating an issue
Before creating an issue, please search for the main keywords to make sure it doesn't already exists. If it does, check if it's still open or not and go through the conversation inside it. You might find there is a lot more context there and some of that can solve your problems.

Only after you've made sure there are no issues already created that match yours, follow these steps:

1. Go to [the issues section](https://gitlab.com/voicemod/kit/control-api-documentation/-/issues) of this repo
2. Click on "Create new issue"
3. Be specific with the title, make sure the main intention behind your problem/request is clear
4. Be even more specific in the body. If you're reporting a problem, **add reproduction steps**. If you're suggesting a change, please explain your motivation for it.

Once the issue is created, feel free to proceed with your local changes while we review the issue.

#### Making local changes & sending the MR
To make the changes, follow these steps:

1. Fork the repo by clicking on the "Fork" button located on the top-right corner of the screen.
2. Make a new branch.
3. Start working and implementing the changes on the newly created branch. Refer to the **Installation & local development** section of this README to understand how to run this project locally. 
4. Once you're happy with the state of your changes, commit them to your fork.
5. Create a Merge Request following [this guide](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork).
6. If you haven't already, update the issue with the link to the MR.

## And you're done!
Now we'll receive the MR and we'll review it, if it meets the issue's description and it passes a code review, we'll merge it!🎉🎉