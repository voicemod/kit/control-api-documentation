## Base ########################################################################
# Use a larger node image to do the build for native deps (e.g., gcc, python)
FROM node:lts as base

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

# optionally if you want to run npm global bin without specifying path
ENV PATH=$PATH:/home/node/.npm-global/bin

# Reduce npm log spam and colour during install within Docker
ENV NPM_CONFIG_LOGLEVEL=warn
ENV NPM_CONFIG_COLOR=false

# Build arguments
ARG public_algolia_app_id
ARG algolia_admin_secret
ARG public_algolia_index
ARG algolia_search_key

ENV PUBLIC_ALGOLIA_APP_ID ${public_algolia_app_id}
ENV ALGOLIA_ADMIN_SECRET ${algolia_admin_secret}
ENV PUBLIC_ALGOLIA_INDEX ${public_algolia_index}
ENV ALGOLIA_SEARCH_KEY ${algolia_search_key}

# We'll run the app as the `node` user, so put it in their home directory
WORKDIR /home/node/app
# Copy the source code over
# COPY --chown=node:node . /home/node/app/
COPY . /home/node/app/


# USER node

## Development #################################################################
# Define a development target that installs devDeps and runs in dev mode
FROM base as development
WORKDIR /home/node/app


# Install (not ci) with dependencies, and for Linux vs. Linux Musl (which we use for -alpine)
RUN npm install
# Switch to the node user vs. root
# Expose port 3000
EXPOSE 3000
# Start the app in debug mode so we can attach the debugger
CMD ["npm", "start"]

## Production ##################################################################
# Also define a production target which doesn't use devDeps
FROM base as production
WORKDIR /home/node/app
RUN npm install -g @asyncapi/generator
# COPY --chown=node:node --from=development /home/node/app/node_modules /home/node/app/node_modules
COPY --from=development /home/node/app/node_modules /home/node/app/node_modules

# Build the Docusaurus app
RUN npm run build

## Deploy ######################################################################
# Use a stable nginx image
FROM nginx:stable-alpine as deploy
WORKDIR /home/node/app
# Copy what we've installed/built from production
# COPY --chown=node:node --from=production /home/node/app/build /usr/share/nginx/html/
COPY --from=production /home/node/app/build /usr/share/nginx/html/
